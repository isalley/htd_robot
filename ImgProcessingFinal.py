import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import time
from time import sleep
import serial
###import RPi.GPIO as GPIO
###import picamera
ser = serial.Serial('/dev/ttyACM0',9600)

# uncomment the following lines if taking picture and analyzing it
import RPi.GPIO as GPIO
import picamera
camera = picamera.PiCamera() # needs to be global

# ---------------------------------- Take Picture  -----------------------------------
def captureImage(image_name, at_home):
    
    if at_home == True:
        img = cv2.imread(('photos/' + image_name))
        tmp = np.shape(img)
        print("original image size:")
        print(tmp)
    else:
        # image_name should be the name of the image including the file extension
        camera.brightness = 50
        #camera.contrast = 0
        #camera.saturation = 0
        #camera.exposure_mode = 'backlight'
        height = 600
        width = 600
        camera.awb_mode = 'fluorescent' #'sunlight'
        camera.resolution = (height, width)
        
        
        # Create an array representing a same size image of a cross through the center of the display.
        # The shape of the array must be of the form (height, width, color)
        '''a = np.zeros((height, width, 3), dtype=np.uint8)
        a[360, :, :] = 0xff
        a[:, 640, :] = 0xff'''
        
        camera.start_preview()
        #o = camera.add_overlay(np.getbuffer(a), layer=3, alpha=64)
        sleep(5)
        camera.capture('photos/'+ image_name)
        #camera.capture('photos/'+ image_name, resize=(320, 240))
        #camera.remove_overlay(o)
        camera.stop_preview()
        img = cv2.imread(('photos/' + image_name))
    
    # resize image to standard 300x300 -- way to "zoom" in on the face without loosing image clarity
    '''tmp = np.shape(img)
        cropped = img[0:301, 150:451]
        #cropped = img0[200:501, 200:501] # hallway dan
        #r,c = 400.0 / shape[img0]
        #   dim = (400, int(np.shape[0] * r))
        #   resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
        cv2.imshow("resized", cropped)
        tmp = np.shape(cropped)
        print("new image size:")
        print(tmp)
        img = cropped'''
    
    return img

# ------------------------------ Apply Edge Detection ------------------------------

def applyEdgeDetection(img):
    
    sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=3)  # x switched from kernel size 5 to size 3
    sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=3)  # y
    sobelx = np.uint8(np.absolute(sobelx)) # converts to black and white image
    sobely = np.uint8(np.absolute(sobely))
    plt.imsave('photos_results/sobelx.png', sobelx, cmap = 'gray')
    plt.imsave('photos_results/sobely.png', sobely, cmap = 'gray')
    sobel_img = cv2.bitwise_or(sobelx, sobely)
    
    return sobel_img

# ----------- PREVIOUS RENDITION -----------
def applyEdgeDetection_Prev(img):
    
    # laplacian
    laplacian = cv2.Laplacian(img,cv2.CV_64F)
    laplacian = np.uint8(np.absolute(laplacian))
    
    # canny
    cannyIm = cv2.Canny(np.uint8(img), 50, 100,3)
    
    # prewitt
    kernelx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
    kernely = np.array([[-1,0,1],[-1,0,1],[-1,0,1]])
    prewittx = cv2.filter2D(img, -1, kernelx)
    prewitty = cv2.filter2D(img, -1, kernely)
    prewittx = np.uint8(np.absolute(prewittx)) # absolute value and essentially converts to black and white image
    prewitty = np.uint8(np.absolute(prewitty))
    prewitt = cv2.bitwise_or(prewittx, prewitty)
    
    # sobel
    sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=3)  # x switched from kernel size 5 to size 3
    sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=3)  # y
    #plt.imsave('photos_results/sobelx.png', sobelx, cmap = 'gray')
    #plt.imsave('photos_results/sobely.png', sobely, cmap = 'gray')
    sobelx = np.uint8(np.absolute(sobelx)) # converts to black and white image
    sobely = np.uint8(np.absolute(sobely))
    sobel = cv2.bitwise_or(sobelx, sobely)
    # cannyImX = cv2.Canny(np.uint8(sobelx), 50, 200)
    # cannyImY = cv2.Canny(np.uint8(sobely), 50, 200)
    
    return sobel, laplacian, prewitt, cannyIm

# --------------------------------- Remove Noise ----------------------------------- ***********
# Remove Noise - white specks in black background
def removeNoise(img_in, img_orig):
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
    r, thresh1 = cv2.threshold(img_in,70,255,0)#70
    r, thresh2 = cv2.threshold(img_in,60,255,0)#50
    r, thresh3 = cv2.threshold(img_in,15,255,0)
    threshC = custAdaptiveFilt(img_in)
    #threshG = cv2.adaptiveThreshold(img_in,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    plt.imsave('photos_results/rm_threshold1.png', thresh1, cmap = 'gray')
    plt.imsave('photos_results/rm_threshold2.png', thresh2, cmap = 'gray')
    plt.imsave('photos_results/rm_threshold3.png', thresh3, cmap = 'gray')
    plt.imsave('photos_results/rm_thresholdC.png', threshC, cmap = 'gray')
    #plt.imsave('photos_results/rm_thresholdG.png', threshG, cmap = 'gray')
    
    #bitOR = thresh1
    bitOR = cv2.bitwise_or(thresh1, thresh2)
    plt.imsave('photos_results/rm_bitOR.png', bitOR, cmap = 'gray')
    bitNOT = cv2.bitwise_not(bitOR)
    plt.imsave('photos_results/rm_bitNOT.png', bitNOT, cmap = 'gray')
    bitOR = cv2.morphologyEx(bitOR, cv2.MORPH_CLOSE, np.ones((1,1),'uint8'))
    plt.imsave('photos_results/rm_morph.png', bitOR, cmap = 'gray')
    #bitOR = cv2.erode(bitOR,kernel,iterations = 1)
    #bitOR = cv2.bitwise_or(bitOR, thresh3)
    #plt.imsave('photos_results/rm_bitOR.png', bitOR, cmap = 'gray')
    
    
    # if there is very little color variation there will be a low threshold required to get image
    tmp = np.shape(img_in)
    ave_pix_val = np.sum(img_in)/tmp[0]/tmp[1]
    print('average pixel value')
    print(ave_pix_val)
    #if(ave_pix_val < 15):
    #bitOR = thresh3
    #bitOR = cv2.dilate(bitOR,kernel,iterations = 2)
    #elif(ave_pix_val > 23):
    #bitOR = thresh1
    
    
    #r, bitOR = cv2.threshold(img_in,110,255,0)#70,100 # investigating lower sampling
    #plt.imsave('photos_results/rm_bitOR.png', bitOR, cmap = 'gray')
    
    return threshC

# --------------------------------- Adaptive Filter -----------------------------------
def custAdaptiveFilt(img):
    # initialize values
    shape_orig = np.shape(img)
    r = shape_orig[0]
    c = shape_orig[1]
    row = 0;
    thresh = np.zeros(shape_orig)+255
    box_width = 50
    
    # pass averaging filter over image
    for j in range(box_width-1,r,box_width): # row index - iterate down the image
        col = 0
        #print(j)
        #print(j-box_width+1)
        for n in range(box_width-1,c,box_width): # col index - iterate across the row
            ave_pix_val = np.sum(img[j-box_width+1:j, n-box_width+1:n])/box_width**2
            print(ave_pix_val)
            row_start = j-box_width+1
            col_start = n-box_width+1
##            cv2.imshow('contours', img[row_start:j+1, col_start:n+1])
##            cv2.waitKey(0)
            # end col and row must index +1 from desired stopping point
            if(ave_pix_val < 5):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],30,255,0)
            elif(ave_pix_val < 8):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],20,255,0)
            elif(ave_pix_val < 14):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],30,255,0)
            elif(ave_pix_val < 20):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],40,255,0)
            elif(ave_pix_val < 25):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],60,255,0)
            elif(ave_pix_val < 30):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],70,255,0)
            elif(ave_pix_val < 30):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],80,255,0)
            elif(ave_pix_val < 40):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],80,255,0)
            elif(ave_pix_val < 50):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],100,255,0)
            else:
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],100,255,0)
            col = col + 1
        row = row + 1
    
    
    thresh = np.uint8(thresh)
    return thresh


# PREVIOUS VERSION OF REMOVE NOISE
def saveRN():
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
    img = cv2.dilate(img_in,kernel,iterations = 2)
    plt.imsave('photos_results/rm_dilated.png', img, cmap = 'gray')
    img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, np.ones((1,1),'uint8'))
    plt.imsave('photos_results/rm_morph1.png', img, cmap = 'gray')
    r, thresh = cv2.threshold(img_in,50,200,0)
    plt.imsave('photos_results/rm_threshold.png', thresh, cmap = 'gray')
    img_out = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, np.ones((3,3),'uint8'))
    plt.imsave('photos_results/rm_morph2.png', img_out, cmap = 'gray')
    #img_out = cv2.dilate(img_out,kernel,iterations = 1)
    # isolate edges
    #canny_out, canny_down = AAF(canny_erode, downsample_factor)
    #img = cv2.Canny(np.uint8(img_in), 50, 110,3)
    return img_out



# ---------------------------------- Get Contours ----------------------------------
def getContours(img):
    # contour
    #ret, thresh = cv2.threshold(img, 1, 50, 0)
    im2, contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    #print(hierarchy)
    return contours



# ------------------------ Determine Contour Drawing Order -------------------------
# order contours longest to shortest
def rearrangeContours(contours):
    
    num_contours = len(contours)
    print('number of contours')
    print(num_contours)
    # initialize array with first contour's length
    cnt_lens = len(contours[0])
    # get length of remaining contours
    for n in range(1,num_contours-1):
        cnt_lens = np.hstack([cnt_lens,-1*len(contours[n])]) # mult by -1 so that sort makes ascending order be descending magnitude
    
    # sort from longest to shortest contour
    descending_order = np.argsort(cnt_lens) # returns original index of the sorted value

    #sorted_lengths = np.sort(cnt_lens) # returns the contours lengths from (longest to shortest)*-1
    #print('sorted contour lengths')
    #print(sorted_lengths)#***************************

    # build new list of arrays of coordinates -- keep only those larger than threshold min length
    sorted_contours = [contours[descending_order[0]], contours[descending_order[1]]]
    for m in range(2,num_contours-1):
        cnt = contours[descending_order[m]]
        if(len(cnt) > 50): # 50 seems to be a good number so far
            sorted_contours.extend([cnt])
    print('length of sorted contours')
    print(len(sorted_contours))
    return sorted_contours



# --------------------------------- Draw Contours ---------------------------------- ***********
def drawConts(sorted_contours,img,img0):
    
    #img0 = cv2.cvtColor(img0, cv2.COLOR_BGR2RGB)
    print('about to draw contours')
    # DRAW ALL CONTOURS ON ORIGINAL IMAGE
    cv2.drawContours(img0, sorted_contours, -1, (0,255,0), 2)
    plt.imsave('photos_results/with_contours.png', img0)

    tmp = np.shape(img0)
    mask3 = np.zeros((tmp[0], tmp[1],3), np.uint8)
    cv2.drawContours(mask3, sorted_contours, -1, (0,255,0), 2)
    plt.imsave('photos_results/final_cnt_on_blank.png', mask3)

    print('contours drawn')

# --------------------------- *****Send Contours***** ---------------------------
def sendContours(contours):
    
    num_contours = len(contours)
    delay_len = 0.8 # in seconds
    
    # tell the Arduino to prepare for a new image to be sent
    new_pic_available = 'n'
    ser.write(new_pic_available.encode())
    sleep(delay_len)
    final_end = 'd'
    k = 0
    
    # send all contours pixel by pixel
    for n in range(0,num_contours): # loop contours
        cnt = contours[n]
        cnt_len = len(cnt)
        begin_char = 'b'
        end_char = 'e'
        ser.write(begin_char.encode())
        print(n)
        #sleep(2)
        for m in range(0,cnt_len):
            if(k>=10):
                if(cnt_len < 70):
                    if(m % 2 == 0 or m == cnt_len-1):
                        str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                        ser.write(str_msg.encode())
                        sleep(0.25)
                        k = k+1
                elif(cnt_len < 200):
                    if(m % 5 == 0 or m == cnt_len-1):
                        str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                        ser.write(str_msg.encode())
                        sleep(0.25)
                        k = k+1
            
                elif(m % 10 == 0 or m == cnt_len-1):
                    str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                    ser.write(str_msg.encode())
                    sleep(0.25)
                    k = k+1              
            else:
                if(m % 2 == 0 or m == cnt_len-1):
                    str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                    ser.write(str_msg.encode())
                    sleep(0.8)
                    k = k+1
        ser.write(end_char.encode())
        sleep(2.5)
        k = 0
    print(n)
    ser.write(final_end.encode())

# return nothing


# ----------------------------------- Save Images ------------------------------------
def saveFinalImage(edge_orig, final_im, filter_name):
    plt.imsave('photos_results/final_' + filter_name + '.png', final_im, cmap = 'gray')
    plt.imsave('photos_results/final_pre_' + filter_name + '.png', edge_orig, cmap = 'gray')


# ----------------------------------- Image Processing ------------------------------------ ***********
def processImg(at_home, image_name):
    # take picture with rasPi Camera
    # preferred 1,3,6
    if(at_home == True):
        #img0 = captureImage('original_image.jpg', at_home)
        img0 = captureImage('circle.jpg', at_home) # 1 = Izzy, 2 = Meghna, 3 = Thaer, 8 = Blake, 9 = Basil, 10 = Socrates
    #img0 = captureImage('Izzy_test1.jpg', at_home)
    #img0 = captureImage('Dan.jpg', at_home)
    else:
        img0 = captureImage(image_name,at_home)
    
    # converting to gray scale and convert to floating point number 0-1
    im_gray = cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)
    #print(np.max(im_gray))
    #im_gray = cv2.normalize(im_gray.astype('float'), None, 0.0, 1.0, cv2.NORM_MINMAX)
    #print(np.max(im_gray))

    # filter and downsample
    #downsample_factor = 3
    #im_filt, im_down = AAF(im_gray, downsample_factor)
    #im_filt = cv2.bilateralFilter(im_filt,3,200,20)
    #gray = cv2.bilateralFilter(gray, 11, 17, 17)
    #im_down = cv2.GaussianBlur(im_down,(2,2),0)

    # detect edges -- Options: sobel, laplacian, prewitt, cannyIm
    edge = applyEdgeDetection(im_gray)
    sobel1, laplacian1, prewitt1, canny1 = applyEdgeDetection_Prev(im_gray) # apply edge detector to original image
    #sobel2, laplacian2, prewitt2, canny2 = applyEdgeDetection_Prev(im_filt) # apply edge detector to AAF filtered image
    #sobel3, laplacian3, prewitt3, canny3 = applyEdgeDetection_Prev(im_down) # apply edge detector to AAF filtered/downsampled image
    
    # select input to use in following processing
    #edge_orig = sobel1
    #edge_filt = sobel2
    
    # Remove Nosie  -- threshold and erode/dilate
    final_im = removeNoise(edge, img0)
    #final_canny = removeNoise(canny1)
    #final_sobel = removeNoise(sobel1)
    
    # save final image
    saveFinalImage(edge, final_im, 'filtered_final')
    saveFinalImage(canny1, canny1, 'canny')
    #saveFinalImage(sobel1, final_sobel, 'sobel')
    
    # evaluate contours
    contours = getContours(final_im)
    sorted_contours = rearrangeContours(contours)
    drawConts(sorted_contours, final_im, img0)
    sendContours(sorted_contours)
    
    # evaluate contours
    '''contours = getContours(canny1)
        sorted_contours = rearrangeContours(contours)
        drawConts(sorted_contours, final_im, img0)
        printContours(sorted_contours)'''
    
    
    print("image processing finished")



#----------------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------- Main -----------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------
def main():
    counter = 0
    at_home = False
    date = '5' # UPDATE WHEN TAKING PICTURES *******
    counter = 0 # UPDATE WHEN TAKING PICTURES *******
    
    # for working on image processing without pi
    if(at_home == True):
        image_name = 'holder'
        processImg(at_home, image_name)
    # for working with access to the pi
    
    else:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(14, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        print('ready: press button to take picture')
        while(True):
            # take picture with raspPi Camera
##            img_name = 'testCircle.jpg'
            img_name = 'april' + str(date) +'_test' + str(counter) + '.jpg'
            input_state = GPIO.input(14)
            if input_state == False:
                processImg(at_home, img_name)
                counter = counter + 1
                print('ready: press button to take picture')

if __name__ == "__main__":
    main()



