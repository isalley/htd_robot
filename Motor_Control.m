%% load robot position based on contours
close all

% INITIALIZE VARIABLES
num_contours = 3;
x_output = [];
y_output = [];
x_input = [];
y_input = [];
zero_pad = zeros(1,10);
y_pad = ones(1,10)*400;

%% GENERATE EXAMPLE INPUT CONTOURS / FORMAT CONTOURS INPUT FROM RPI
% circle
        radius=10;
        center=[90 237];
        theta=0:2*pi/360:2*pi;
        x_circ = radius*cos(theta)+center(1);
        y_circ = radius*sin(theta)+center(2);
        
pixel_locationY = {[400,225:-1:175,175:1:200,201:-1:150,150:200,y_pad],...
                   [400,y_circ,y_pad],...
                   [400, 50:69, 69*ones(1,10), 69:-1:50, 50*ones(1,10),50:69, y_pad]};
pixel_locationX = {[0,20:96,96:172,172:-1:147,zero_pad],...
                   [0,x_circ,zero_pad]...
                   [0, 100*ones(1,20), 100:109, 109*ones(1,20), 109:118, 118*ones(1,20), zero_pad]};
         
               
               
%% BUILD TRANSFER FUNCTIONS               
% build precompensator transfer function
s = tf('s');
G_precomp = 1/(5*s+1)/(2*s+1); % overdamp the input

% build PID transfer function
kp = 1;
ki = 0;
kd = 0;
G_PID = ki/s + kd*s + kp; % potential options phase lead or phase lag compensators
G_robot = (10*s+1)/(100+s_1)/(0.01*s+1);
H = 1; % sensor gain
G_loop = G_PID*G_robot/(1+H*G_PID*G_robot);

% might have to frequency sweep input sinusoids to determine system dynamics transfer function
% then use nyquist to evaluate to how to pull the poles into LHP



%% APPLY INPUT TO CONTROL AND OUTPUT TO ROBOT WITH FEEDBACK
for n = 1:num_contours
    
    len_contour = length(pixel_locationX{n});
    X = [];
    Y = [];
    % save all contour x values to a single variable vector
%     for m = 1:len_contour
%         X = [X pixel_locationX{n}{m}(1)];
%         Y = [Y pixel_locationY{n}{m}(1)];
%     end
    
    % save all contour x values to a single variable vector
    for m = 1:len_contour
        X = [X pixel_locationX{n}(m)];
        Y = [Y pixel_locationY{n}(m)];
    end
    
    % save contour into input signal vector -- used later in graphing
    % results comparison
%     x_coor = [x_coor pixel_locationX{n}{1:len_contour}(1)];
%     y_coor = [y_coor pixel_locationY{n}{1:len_contour}(1)];
    x_input = [x_input X];
    y_input = [y_input Y];
    
    % scale pixel location input to be an angluar position of the motor
    A = 360/400; % gain from contour location to angular motor position
    B = 8/360; % gain from angular position to real space inches position
    control_inputY = (400-Y)*A;
    control_inputX = X*A;

    % apply precompensator to the input 
    control_outputX = lsim(G_precomp, control_inputX, 0:length(control_inputX)-1);
    control_outputY = lsim(G_precomp, control_inputY, 0:length(control_inputX)-1);
    
    % apply the PID transfer function to the x and y error signals

    % saturate values so that the robot is not asked to move to a noexistant
    % position
    maxY = 360;
    maxX = 360;
    for n = 1:len_cont
        if control_outputY(n) > maxY
            control_outputY(n) = maxY;
        elseif control_outputY(n) < 0
            control_outputY(n) = 0;
        end
        if control_outputX(n) > maxX
            control_outputX(n) = maxX;
        elseif control_outputX(n) < 0
            control_outputX(n) = 0;
        end
    end
    
    % first apply step input of first contour location
    while(errorX > 10 || errorY > 10)
        errorX = real_spaceX(1) - meas;
        errorY = real_spaceY(1) - meas;
    end
    
    % apply remaining contour as input
    for m = 1:len_contour
        while(errorX > 1 || errorY > 1)
            errorX = real_spaceX(m) - meas;
            errorY = real_spaceY(m)- meas;

            if (errorX >= 0 && errorY >= 0)
                % have motors move right/up
            elseif (errorX >= 0 && errorY < 0)
                % have motor move right/down
            elseif (errorX < 0 && errorY >= 0)
                % have motor move left/up
            else
                % have motor move left/down
            end
        end
    end
    
    % return robot to (0,0) with step input
    while(errorX > 10 || errorY > 10)
        errorX = 0 - meas;
        errorY = 0 - meas;
    end
    
    % ----- FOR VISUALIZING OUTPUT -----
    % scale control output to be the real space paper coordinate
    real_spaceX = control_outputX'*B;
    real_spaceY = control_outputY'*B;

    % add the real space paper coordinate to the travel path
    x_output = [x_output real_spaceX];
    y_output = [y_output real_spaceY];
    x_len = length(x_output);
    
end

%% plot output of control of robot's position
figure(1)
subplot(2,1,1);plot(pixel_locationX, 400-pixel_locationY,'b*-'); title('Input');axis([0 400 0 400]);
subplot(2,1,2); plot(x_output, y_output, 'r*-'); xlabel('X Position');ylabel('Y Position'); title('Control Output'); axis([0 8 0 8]);
print('Overall_control','-dpng');

figure(2)
subplot(2,1,1);plot(1:length(y_input), 400-y_input,'b*-'); ylabel('Input X Coordinate');
subplot(2,1,2); plot(1:length(y_output), y_output, 'r*-'); xlabel('Time Index');ylabel('Control Output X Coordinate');
print('X_control','-dpng');

figure(3)
subplot(2,1,1);plot(1:length(x_input), x_input,'b*-'); ylabel('Input Y Coordinate');
subplot(2,1,2); plot(1:length(x_output),x_output, 'r*-'); xlabel('Time Index'); ylabel('Control Output Y Coordinate');
print('Y_control','-dpng');

