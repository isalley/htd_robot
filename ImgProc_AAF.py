import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import time
from time import sleep
#import RPi.GPIO as GPIO
#import picamera

#camera = picamera.PiCamera() # needs to be global

# ---------------------------------- Take Picture  -----------------------------------
def captureImage(image_name, at_home):
    
    if at_home == True:
        img = cv2.imread(('photos/' + image_name))
        tmp = np.shape(img)
        print("original image size:")
        print(tmp)
    else:
        # image_name should be the name of the image including the file extension
        camera.brightness = 50
        camera.contrast = -20
        camera.saturation = 30
        #camera.exposure_mode = 'backlight'
        height = 600
        width = 600
        #camera.awb_mode = 'fluorescent' #'sunlight'
        camera.resolution = (height, width)
        
        # Create an array representing a same size image of a cross through the center of the display.
        # The shape of the array must be of the form (height, width, color)
        '''a = np.zeros((height, width, 3), dtype=np.uint8)
        a[360, :, :] = 0xff
        a[:, 640, :] = 0xff'''
        
        camera.start_preview()
        #o = camera.add_overlay(np.getbuffer(a), layer=3, alpha=64)
        sleep(5)
        camera.capture('photos/'+ image_name)
        #camera.capture('photos/'+ image_name, resize=(320, 240))
        #camera.remove_overlay(o)
        camera.stop_preview()

    # resize image to standard 300x300 -- way to "zoom" in on the face without loosing image clarity
    '''tmp = np.shape(img)
    cropped = img[0:301, 150:451]
    #cropped = img0[200:501, 200:501] # hallway dan
    #r,c = 400.0 / shape[img0]
    #   dim = (400, int(np.shape[0] * r))
    #   resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
    cv2.imshow("resized", cropped)
    tmp = np.shape(cropped)
    print("new image size:")
    print(tmp)
    img = cropped'''

    return img

# ---------------------------------- Apply Filter -----------------------------------
# this applies a NxN filter to an image (adapted from code written for HW2)
def AAF(img, downsample_factor):
    
    # build filter
    filter_size = (downsample_factor, downsample_factor)
    filter = np.ones(filter_size)/(downsample_factor**2)
    
    # initialize values
    shape_filt = np.shape(filter)
    h = shape_filt[0]
    w = shape_filt[1]
    shape_orig = np.shape(img)
    r = shape_orig[0]
    c = shape_orig[1]
    row = 0;
    im_filt = np.zeros(shape_orig)
    
    # pass averaging filter over image
    for j in range(h,r-1,h): # row index - iterate down the image
        col = 0;
        for n in range(w,c-1,w): # col index - iterate across the row
            im_filt[j-h:j, n-w:n] = (img[j-h:j,n-w:n] * filter).sum() # must index +1 from desired stopping point
            col = col + 1
        row = row + 1
    
    # downsample image
    shape_im_filt = np.shape(im_filt)
    num_rows = np.size(im_filt[0:shape_im_filt[0]:downsample_factor, 1])
    num_cols = np.size(im_filt[1, 0:shape_im_filt[1]:downsample_factor])
    im_down = np.ones((num_rows, num_cols))
    im_down[:,:] = img[0:shape_im_filt[0]:downsample_factor, 0:shape_im_filt[1]:downsample_factor]
    return im_filt, im_down


# ----------- PREVIOUS RENDITION -----------
def applyEdgeDetection_Prev(img):
    
    # laplacian
    laplacian = cv2.Laplacian(img,cv2.CV_64F)
    laplacian = np.uint8(np.absolute(laplacian))
    
    # canny
    cannyIm = cv2.Canny(np.uint8(img), 50, 100,3)
    
    # prewitt
    kernelx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
    kernely = np.array([[-1,0,1],[-1,0,1],[-1,0,1]])
    prewittx = cv2.filter2D(img, -1, kernelx)
    prewitty = cv2.filter2D(img, -1, kernely)
    prewittx = np.uint8(np.absolute(prewittx)) # absolute value and essentially converts to black and white image
    prewitty = np.uint8(np.absolute(prewitty))
    prewitt = cv2.bitwise_or(prewittx, prewitty)
    
    # sobel
    sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=3)  # x switched from kernel size 5 to size 3
    sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=3)  # y
    #plt.imsave('photos_results/sobelx.png', sobelx, cmap = 'gray')
    #plt.imsave('photos_results/sobely.png', sobely, cmap = 'gray')
    sobelx = np.uint8(np.absolute(sobelx)) # converts to black and white image
    sobely = np.uint8(np.absolute(sobely))
    sobel = cv2.bitwise_or(sobelx, sobely)
    # cannyImX = cv2.Canny(np.uint8(sobelx), 50, 200)
    # cannyImY = cv2.Canny(np.uint8(sobely), 50, 200)
    
    # apply canny to edge detected
    # create binary image from grayscale
    '''t = 20 # threshold -- **** the trackbars to adjust this in real time might be a really good idea!!! ****
        (t, sobelxy) = cv2.threshold(sobelxy, t, 200, cv2.THRESH_BINARY_INV)
        (t, binaryY) = cv2.threshold(sobely, t, 255, cv2.THRESH_BINARY_INV)
        cannyXY = cv2.bitwise_or(cannyImX, cannyImY) # combined x and y
        plt.figure(), plt.imshow(sobel,cmap = 'gray'), plt.title('Bitwise Or')
        cannyXY = cv2.bitwise_or(cannyImX, cannyImY) # combined x and y
        #plt.figure(), plt.imshow(sobel,cmap = 'gray'), plt.title('Bitwise Or')
        '''
    
    return sobel, laplacian, prewitt, cannyIm

# --------------------------------- Remove Noise ----------------------------------- ***********
    # Remove Noise - white specks in black background
def removeNoise(img_in, img_orig):
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
    r, thresh1 = cv2.threshold(img_in,70,255,0)#70
    r, thresh2 = cv2.threshold(img_in,60,255,0)#50
    r, thresh3 = cv2.threshold(img_in,15,255,0)
    threshC = custAdaptiveFilt(img_in)
    #threshG = cv2.adaptiveThreshold(img_in,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    plt.imsave('photos_results/rm_threshold1.png', thresh1, cmap = 'gray')
    plt.imsave('photos_results/rm_threshold2.png', thresh2, cmap = 'gray')
    plt.imsave('photos_results/rm_threshold3.png', thresh3, cmap = 'gray')
    plt.imsave('photos_results/rm_thresholdC.png', threshC, cmap = 'gray')
    #plt.imsave('photos_results/rm_thresholdG.png', threshG, cmap = 'gray')
    
    #bitOR = thresh1
    bitOR = cv2.bitwise_or(thresh1, thresh2)
    plt.imsave('photos_results/rm_bitOR.png', bitOR, cmap = 'gray')
    bitNOT = cv2.bitwise_not(bitOR)
    plt.imsave('photos_results/rm_bitNOT.png', bitNOT, cmap = 'gray')
    bitOR = cv2.morphologyEx(bitOR, cv2.MORPH_CLOSE, np.ones((1,1),'uint8'))
    plt.imsave('photos_results/rm_bitOR_morph.png', bitOR, cmap = 'gray')
    bitOR = cv2.erode(bitOR,kernel,iterations = 1)
    #bitOR = cv2.bitwise_or(bitOR, thresh3)
    #plt.imsave('photos_results/rm_bitOR.png', bitOR, cmap = 'gray')
    
    #threshC = cv2.morphologyEx(threshC, cv2.MORPH_CLOSE, np.ones((2,2),'uint8'))
    #plt.imsave('photos_results/threshC_morph.png', threshC, cmap = 'gray')
    
    
     # if there is very little color variation there will be a low threshold required to get image
    tmp = np.shape(img_in)
    ave_pix_val = np.sum(img_in)/tmp[0]/tmp[1]
    print('average pixel value')
    print(ave_pix_val)
    #if(ave_pix_val < 15):
    #bitOR = thresh3
    #bitOR = cv2.dilate(bitOR,kernel,iterations = 2)
    #elif(ave_pix_val > 23):
    #bitOR = thresh1


    #r, bitOR = cv2.threshold(img_in,110,255,0)#70,100 # investigating lower sampling
    #plt.imsave('photos_results/rm_bitOR.png', bitOR, cmap = 'gray')

    #boundary_contour = getMask(thresh3, img_orig)
    threshC = cv2.erode(threshC,kernel,iterations = 2)
    
    return bitOR, threshC

def custAdaptiveFilt(img): ##################################################
    # initialize values
    shape_orig = np.shape(img)
    r = shape_orig[0]
    c = shape_orig[1]
    row = 0;
    thresh = np.zeros(shape_orig)+255
    box_width = 50
    
    # pass averaging filter over image
    for j in range(box_width-1,r,box_width): # row index - iterate down the image
        col = 0
        #print(j)
        #print(j-box_width+1)
        for n in range(box_width-1,c,box_width): # col index - iterate across the row
            ave_pix_val = np.sum(img[j-box_width+1:j, n-box_width+1:n])/box_width**2
            print(ave_pix_val)
            row_start = j-box_width+1
            col_start = n-box_width+1
            #cv2.imshow('contours', img[row_start:j+1, col_start:n+1])
            #cv2.waitKey(0)
            # end col and row must index +1 from desired stopping point
            if(ave_pix_val < 5):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],50,255,0)
            elif(ave_pix_val < 8):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],20,255,0)
            elif(ave_pix_val < 20):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],30,255,0)
            elif(ave_pix_val < 40):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],40,255,0)
            elif(ave_pix_val < 50):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],60,255,0)
            elif(ave_pix_val < 60):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],80,255,0)
            else:
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],100,255,0)
            col = col + 1
        row = row + 1


    '''elif(ave_pix_val < 14):
    r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],30,255,0)
    elif(ave_pix_val < 20):
    r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],50,255,0)
    elif(ave_pix_val < 23):
    r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],60,255,0)
    elif(ave_pix_val < 28):
    r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],60,255,0)
    elif(ave_pix_val < 33):
    r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],70,255,0)
    elif(ave_pix_val < 45):
    r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],80,255,0)
    elif(ave_pix_val < 55):
    r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],100,255,0)'''

    thresh = np.uint8(thresh)
    return thresh

# ----------------------------------- GET MASK -----------------------------------
def getMask(img,img_orig):
    # get very distinct outline -- should be done with the third contour
    h, w = img.shape[:2]
    mask1 = np.zeros((h+2, w+2), np.uint8)
    mask2 = np.zeros((h+2, w+2), np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
    
    #img = cv2.dilate(img,kernel,iterations = 1)
    img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, np.ones((3,3),'uint8'))
    plt.imsave('photos_results/rm_dilated.png', img, cmap = 'gray')
    
    
    cv2.floodFill(img, mask1, (0,0), 255);
    cv2.floodFill(img, mask2, (h-1,0), 255);
    m1_inv = cv2.bitwise_not(mask1)
    m2_inv = cv2.bitwise_not(mask2)
    im_floodfill_inv = cv2.bitwise_and(m1_inv, m2_inv)
    cropped = im_floodfill_inv[3:h,3:w]
    plt.imsave('photos_results/rm_filled1.png', mask1, cmap = 'gray')
    plt.imsave('photos_results/rm_filled2.png', mask2, cmap = 'gray')
    plt.imsave('photos_results/rm_filled.png', cropped, cmap = 'gray')
    
    
    #img_out = cv2.morphologyEx(thresh3, cv2.MORPH_CLOSE, np.ones((10,10),'uint8'))
    #plt.imsave('photos_results/rm_morph2.png', img_out, cmap = 'gray')
    boundary_contour = getContours(cropped)
    
    tmp = np.shape(im_floodfill_inv)
    mask3 = np.zeros((tmp[0], tmp[1],3), np.uint8)
    cv2.drawContours(img_orig, boundary_contour, -1, (0,255,0), 2)
    img_orig = cv2.cvtColor(img_orig, cv2.COLOR_BGR2RGB)
    plt.imsave('photos_results/rm_head_boundary.png', img_orig)
    #cv2.imshow("cropped", img_in)
    #cv2.waitKey(0)

    #stacked = np.dstack([[im_floodfill_inv],[im_floodfill_inv],[im_floodfill_inv]])
    #sort_big = rearrangeContours(cnt_big, im_floodfill_inv, img_in)
    print('outline num contours')
    print(len(boundary_contour))
    
    return boundary_contour



# PREVIOUS VERSION OF REMOVE NOISE
def saveRN(img_in):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
    img = cv2.dilate(img_in,kernel,iterations = 2)
    plt.imsave('photos_results/rm_dilated.png', img, cmap = 'gray')
    img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, np.ones((1,1),'uint8'))
    plt.imsave('photos_results/rm_morph1.png', img, cmap = 'gray')
    r, thresh = cv2.threshold(img_in,50,200,0)
    plt.imsave('photos_results/rm_threshold.png', thresh, cmap = 'gray')
    img_out = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, np.ones((3,3),'uint8'))
    plt.imsave('photos_results/rm_morph2.png', img_out, cmap = 'gray')
    #img_out = cv2.dilate(img_out,kernel,iterations = 1)
    # isolate edges
    #canny_out, canny_down = AAF(canny_erode, downsample_factor)
    #img = cv2.Canny(np.uint8(img_in), 50, 110,3)
    return img_out



# ---------------------------------- Get Contours ----------------------------------
def getContours(img):
    # contour
    #ret, thresh = cv2.threshold(img, 1, 50, 0)
    im2, contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    #print(hierarchy)
    return contours


# ------------------------ Determine Contour Drawing Order -------------------------
# order contours longest to shortest
def rearrangeContours(contours):
    
    num_contours = len(contours)
    print('number of contours')
    print(num_contours)
    # initialize array with first contour's length
    cnt_lens = len(contours[0])
    # get length of remaining contours
    for n in range(1,num_contours-1):
        cnt_lens = np.hstack([cnt_lens,-1*len(contours[n])]) # mult by -1 so that sort makes ascending order be descending magnitude

    # sort from longest to shortest contour
    descending_order = np.argsort(cnt_lens) # returns original index of the sorted value

    sorted_lengths = np.sort(cnt_lens) # returns the contours lengths from (longest to shortest)*-1
    print('sorted contour lengths')
    print(sorted_lengths)#***************************

    # build new list of arrays of coordinates -- keep only those larger than threshold min length
    sorted_contours = [contours[descending_order[0]], contours[descending_order[1]]]
    for m in range(2,num_contours-1):
        cnt = contours[descending_order[m]]
        if(len(cnt) > 50): # 50 seems to be a good number so far
            sorted_contours.extend([cnt])
    print('length of sorted contours')
    print(len(sorted_contours))
    return sorted_contours



# --------------------------------- Draw Contours ---------------------------------- ***********
def drawConts(sorted_contours,img,img0, name):

#img0 = cv2.cvtColor(img0, cv2.COLOR_BGR2RGB)
    print('about to draw contours')
    # DRAW ALL CONTOURS ON ORIGINAL IMAGE
    cv2.drawContours(img0, sorted_contours, -1, (0,255,0), 2)
    #plt.imsave('photos_results/with_contours.jpg', img0)
    #cv2.imshow('contours', img0)
    #cv2.waitKey(0)
    '''# SHOW INDIVIDUAL CONTOURS
    for m in range(0,2):
        cnt = sorted_contours[m]
        im_color = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
        cv2.drawContours(im_color, [cnt], 0, (0,255,0), 3) # make last argument -1 to shade in contour, otherwise is width of bounding line
        cv2.imshow('contours', im_color)
        cv2.waitKey(0) # click on image and press any key to close it and finish executing script'''

    tmp = np.shape(img)
    mask3 = np.zeros((tmp[0], tmp[1],3), np.uint8)
    cv2.drawContours(mask3, sorted_contours, -1, (0,255,0), 2)
    plt.imsave('photos_results/final_cnt_on_blank_'+name+'.jpg', mask3)

    rect = cv2.minAreaRect(sorted_contours[0])
    box = cv2.boxPoints(rect)
    box = np.int0(box)

    cv2.drawContours(img0,[box],0,(0,0,255),2)
    #cv2.imshow('Bounded Rotated Rectangle', img0), cv2.waitKey(0)

    (x,y),radius = cv2.minEnclosingCircle(sorted_contours[0])
    center = (int(x),int(y))
    radius = int(radius)

    cv2.circle(img0,center,radius,(0,255,0),2)
    #cv2.imshow('Bounded Circle', img0), cv2.waitKey(0)

    ellipse = cv2.fitEllipse(sorted_contours[0])
    cv2.ellipse(img0,ellipse,(255,0,0),2)
    #cv2.imshow('Bounded Rotated Ellipse', img0), cv2.waitKey(0)
    #plt.imsave('photos_results/with_bounding.jpg', img0)
    print('contours drawn')

# ----------------------------------- Save Images ------------------------------------
def saveFinalImage(edge_orig, final_im, filter_name):
    plt.imsave('photos_results/final_' + filter_name + '.jpg', final_im, cmap = 'gray')
    #plt.imsave('photos_results/final_pre_' + filter_name + '.jpg', edge_orig, cmap = 'gray')



# ----------------------------------- Image Processing ------------------------------------ ***********
def processImg(at_home, image_name):
    # take picture with rasPi Camera
    # preferred 1,3,6
    if(at_home == True):
        #img0 = captureImage('original_image.jpg', at_home)
        img0 = captureImage(image_name, at_home) # 1 = Izzy, 2 = Meghna, 3 = Thaer, 8 = Blake, 9 = Basil, 10 = Socrates
        #img0 = captureImage('Izzy_test1.jpg', at_home)
        #img0 = captureImage('wow.png', at_home)
    else:
        img0 = captureImage(image_name, at_home)
    
    # converting to gray scale and convert to floating point number 0-1
    im_gray = cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)
    plt.imsave('photos_results/grayscale.jpg', im_gray, cmap = 'gray')

    # filter and downsample
    downsample_factor = 3
    im_filt, im_down = AAF(im_gray, downsample_factor)
    plt.imsave('photos_results/imFilt.jpg', im_filt, cmap = 'gray')
    plt.imsave('photos_results/imDown.jpg', im_down, cmap = 'gray')
    #im_filt = cv2.bilateralFilter(im_filt,3,200,20)
    #gray = cv2.bilateralFilter(gray, 11, 17, 17)
    #im_down = cv2.GaussianBlur(im_down,(2,2),0)

    # detect edges -- Options: sobel, laplacian, prewitt, cannyIm
    #edge = applyEdgeDetection(im_down)
    #sobel1, laplacian1, prewitt1, canny1 = applyEdgeDetection_Prev(im_gray) # apply edge detector to original image
    #sobel2, laplacian2, prewitt2, canny2 = applyEdgeDetection_Prev(im_filt) # apply edge detector to AAF filtered image
    sobel3, laplacian3, prewitt3, canny3 = applyEdgeDetection_Prev(im_down) # apply edge detector to AAF filtered/downsampled image
    plt.imsave('photos_results/imSob.jpg', sobel3, cmap = 'gray')

    # Remove Nosie  -- threshold and erode/dilate
    final_im= saveRN(sobel3)
    plt.imsave('photos_results/imFinal.jpg', final_im, cmap = 'gray')

    # save final image
    #saveFinalImage(edge, final_im, 'bitOR_final')
    #saveFinalImage(sobel3, final_im, 'sobelAAF')
    #saveFinalImage(canny1, canny1, 'canny')
    #saveFinalImage(sobel1, final_sobel, 'sobel')

    # evaluate contours
    '''contours = getContours(final_im)
    sorted_contours = rearrangeContours(contours)
    drawConts(sorted_contours, final_im, img0, 'orig')
    #printContours(sorted_contours)'''
    
    contours = getContours(final_im)
    sorted_contours = rearrangeContours(contours)
    drawConts(sorted_contours, final_im, img0, 'thresh')
                   
    # evaluate contours
    '''contours = getContours(canny1)
    sorted_contours = rearrangeContours(contours)
    drawConts(sorted_contours, final_im, img0)'''
    #printContours(sorted_contours)
    
    
    print("image processing finished")



#----------------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------- Main -----------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------
def main():
    at_home = True
    date = '27' # UPDATE WHEN TAKING PICTURES *******
    counter = 1 # UPDATE WHEN TAKING PICTURES *******
    
    # for working on image processing without pi
    if(at_home == True):
        #image_name = 'march'+ str(date) +'_test' + str(counter) + '.jpg'
        image_name = 'Team1.jpg'
        processImg(at_home, image_name)
    # for working with access to the pi

    else:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(14, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        print('ready: press button to take picture')
        while(True):
            # take picture with raspPi Camera
            img_name = 'april'+ date +'_test' + counter + '.jpg'
            input_state = GPIO.input(14)
            if input_state == False:
                processImg(at_home, img_name)
                counter = counter + 1
                print('ready: press button to take picture')

if __name__ == "__main__":
    main()



