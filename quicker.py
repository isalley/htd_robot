import cv2 as cv
import cv2
import numpy as np
import picamera

import time
from time import sleep
import serial
import RPi.GPIO as GPIO

import timeit
import time

kernel = np.ones((5,5),np.uint8)

# Take input from webcam
cap = cv2.VideoCapture(0)

#r, still = cap.read()
#cv2.imshow('Test',still)

# Reduce the size of video to 320x240 so rpi can process faster
cap.set(3,800)
cap.set(4,800)

def nothing(x):
    pass
# Creating a windows for later use
#cv2.namedWindow('HueComp')
#cv2.namedWindow('SatComp')
#cv2.namedWindow('ValComp')
cv2.namedWindow('closing')
cv2.namedWindow('tracking')


# Creating track bar for min and max for hue, saturation and value
# You can adjust the defaults as you like
#cv2.createTrackbar('hmin', 'HueComp',12,179,nothing)
#cv2.createTrackbar('hmax', 'HueComp',37,179,nothing)

#cv2.createTrackbar('smin', 'SatComp',96,255,nothing)
#cv2.createTrackbar('smax', 'SatComp',255,255,nothing)

#cv2.createTrackbar('vmin', 'ValComp',186,255,nothing)
#cv2.createTrackbar('vmax', 'ValComp',255,255,nothing)

# My experimental values
hmn = 12
hmx = 37
smn = 145
smx = 255
vmn = 186
vmx = 255


while(1):
    
    #timing
    #start = timeit.timeit()
    

    buzz = 0
    _, frame = cap.read()

    #converting to HSV
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    hue,sat,val = cv2.split(hsv)

    # get info from track bar and appy to result
    #hmn = cv2.getTrackbarPos('hmin','HueComp')
    #hmx = cv2.getTrackbarPos('hmax','HueComp')
    

    #smn = cv2.getTrackbarPos('smin','SatComp')
    #smx = cv2.getTrackbarPos('smax','SatComp')


    #vmn = cv2.getTrackbarPos('vmin','ValComp')
    #vmx = cv2.getTrackbarPos('vmax','ValComp')

    # Apply thresholding
    hthresh = cv2.inRange(np.array(hue),np.array(hmn),np.array(hmx))
    sthresh = cv2.inRange(np.array(sat),np.array(smn),np.array(smx))
    vthresh = cv2.inRange(np.array(val),np.array(vmn),np.array(vmx))

    # AND h s and v
    tracking = cv2.bitwise_and(hthresh,cv2.bitwise_and(sthresh,vthresh))

    # Some morpholigical filtering
    dilation = cv2.dilate(tracking,kernel,iterations = 1)
    closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel)
    closing = cv2.GaussianBlur(closing,(5,5),0)

    # Detect circles using HoughCircles
    circles = cv2.HoughCircles(closing,cv2.HOUGH_GRADIENT,2,120,param1=120,param2=50,minRadius=10,maxRadius=0)
    # circles = np.uint16(np.around(circles))
    
    
    #Draw Circles
    if circles is not None:
            for i in circles[0,:]:
                # If the ball is far, draw it in green
                if int(round(i[2])) < 30:
                    cv2.circle(frame,(int(round(i[0])),int(round(i[1]))),int(round(i[2])),(0,255,0),5)
                    cv2.circle(frame,(int(round(i[0])),int(round(i[1]))),2,(0,255,0),10)
				# else draw it in red
                elif int(round(i[2])) > 35:
                    cv2.circle(frame,(int(round(i[0])),int(round(i[1]))),int(round(i[2])),(0,0,255),5)
                    cv2.circle(frame,(int(round(i[0])),int(round(i[1]))),2,(0,0,255),10)
                    buzz = 1
                print ("(",round(i[0]),", ", round(i[1]), ", ", round(i[2]), ")")

	#you can use the 'buzz' variable as a trigger to switch some GPIO lines on Rpi :)
    # print buzz                    
    # if buzz:
        # put your GPIO line here
        
    #Calculating the moments
    #imgColorProcessed = ColorProcess(frame)
    #mat = cv.GetMat(imgColorProcessed)
    #mat = cv2.GetMat(frame)
    #moments = cv2.Moments(mat,0)
    #area = cv2.GetCentralMoment(moments,0,0)
    #moment10 = cv2.GetSpatialMoment(moments,1,0)
    #moment01 = cv2.GetSpatialMoment(moments,0,1)
    #if (area > 10000):
        #posX = int(moment10/area)
        #posY = int(moment01/area)
        #print ("X Val: ",posX," Y Val: ", posY, " Area: ", area)
        
    #Caluclating the moments
    
    

    #Timing
    #end = timeit.timeit()
    #print (start)
    #print (end)
    #print (end - start)
    #time.sleep(5)
        
    #Grids
    #cv2.line(int(tracking), (tracking.shape[1]/2, 0), (tracking.shape[1]/2, tracking.shape[0]), (255, 0, 0), 1, 1)
    #cv2.circle(tracking,(127,127), 55, (0,255,0), -1) one) change coordinate of middle axis.2) output ball coord's
    #3) overlay grid axes on regular camera to fit paper
        
    #Draw Grid
##    for y in range(0,480,40):
##        cv2.line(closing, (0,y), (640,y),(255,0,0),1)
##        for x in range(0, 640, 40):
##            cv2.line(closing,(x,0),(x,480),(255,0,0),1)
##    cv2.line(frame, (0,240), (640,240),(255,255,0),1)
##    cv2.line(frame, (320,0), (320,480),(255,255,0),1)
        
    for y in range(0,800,40):
        cv2.line(closing, (0,y), (800,y),(255,0,0),1)
        for x in range(0, 800, 40):
            cv2.line(closing,(x,0),(x,800),(255,0,0),1)
    cv2.line(frame, (0,400), (800,400),(255,255,0),1)
    cv2.line(frame, (400,0), (400,800),(255,255,0),1)
    
    #Show the result in frames
    #cv2.imshow('HueComp',hthresh)
    #cv2.imshow('SatComp',sthresh)
    #cv2.imshow('ValComp',vthresh)
    cv2.imshow('closing',cv2.flip(closing,0))
    cv2.imshow('tracking',cv2.flip(frame,0))

    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cap.release()

cv2.destroyAllWindows()