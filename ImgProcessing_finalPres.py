import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import time
from time import sleep

# uncomment the following lines when running at_home
import serial
ser = serial.Serial('/dev/ttyACM0',9600)

# uncomment the following lines if taking picture and analyzing it
import RPi.GPIO as GPIO
import picamera
camera = picamera.PiCamera() # needs to be global

# ---------------------------------- Take Picture  -----------------------------------
def captureImage(image_name, at_home, take_new):
    # image_name should be the name of the image including the file extension
    if at_home == True:
        img = cv2.imread(('expo/' + image_name))
        #tmp = np.shape(img)
        #print("original image size:")
        #print(tmp)
    else:
        camera.brightness = 70
        camera.contrast = 20
        camera.saturation = 70 #-30
        #camera.exposure_mode = 'backlight'
        height = 600
        width = 600
        #camera.awb_mode = 'fluorescent' #'sunlight'
        camera.resolution = (height, width)
        
        if take_new == True:
            print('press button to take picture')
            sleep(2)
            camera.start_preview()
            while(True):
                input_state = GPIO.input(14)
                if input_state == False:
                    
                    camera.capture('expo/'+ image_name)
                    camera.stop_preview()
                    break
                
        img = cv2.imread(('expo/' + image_name))
    
    # resize image to standard 300x300 -- way to "zoom" in on the face without loosing image clarity
    '''tmp = np.shape(img)
        cropped = img[0:301, 150:451]
        #cropped = img0[200:501, 200:501] # hallway dan
        #r,c = 400.0 / shape[img0]
        #   dim = (400, int(np.shape[0] * r))
        #   resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
        cv2.imshow("resized", cropped)
        tmp = np.shape(cropped)
        print("new image size:")
        print(tmp)
        img = cropped'''
    
    return img

# ------------------------------ Apply Edge Detection ------------------------------
def applyEdgeDetection(img):
    
    sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=3)  # x switched from kernel size 5 to size 3
    sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=3)  # y
    sobelx = np.uint8(np.absolute(sobelx)) # converts to black and white image
    sobely = np.uint8(np.absolute(sobely))
    sobel_img = cv2.bitwise_or(sobelx, sobely)
    plt.imsave('expo/EXPO_SOBEL.png', sobel_img, cmap = 'gray')
    
    return sobel_img


# --------------------------------- Remove Noise -----------------------------------
# Remove Noise - white specks in black background
def removeNoise(img_in, img_orig):
    
    threshC = custAdaptiveFilt(img_in)
    #bitOR = cv2.morphologyEx(bitOR, cv2.MORPH_CLOSE, np.ones((1,1),'uint8'))
    plt.imsave('expo/EXPO_THRESH.png', threshC, cmap = 'gray')

    return threshC

# --------------------------------- Adaptive Filter -----------------------------------
def custAdaptiveFilt(img):
    # initialize values
    shape_orig = np.shape(img)
    r = shape_orig[0]
    c = shape_orig[1]
    row = 0;
    thresh = np.zeros(shape_orig)+255
    box_width = 50
    
    # pass averaging filter over image
    for j in range(box_width-1,r,box_width): # row index - iterate down the image
        col = 0
        
        for n in range(box_width-1,c,box_width): # col index - iterate across the row
            ave_pix_val = np.sum(img[j-box_width+1:j, n-box_width+1:n])/box_width**2
            #print(ave_pix_val)
            row_start = j-box_width+1
            col_start = n-box_width+1
            
            if(ave_pix_val < 5):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],40,255,0)
            elif(ave_pix_val < 8):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],20,255,0)
            elif(ave_pix_val < 20):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],40,255,0)
            elif(ave_pix_val < 30):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],50,255,0)
            elif(ave_pix_val < 40):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],60,255,0)
            elif(ave_pix_val < 50):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],70,255,0)
            elif(ave_pix_val < 60):
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],80,255,0)
            else:
                r, thresh[row_start:j+1, col_start:n+1] = cv2.threshold(img[row_start:j+1, col_start:n+1],100,255,0)
            col = col + 1
        row = row + 1
    
    thresh = np.uint8(thresh)
    return thresh


# PREVIOUS VERSION OF REMOVE NOISE
def saveRN():
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2,2))
    img = cv2.dilate(img_in,kernel,iterations = 2)
    plt.imsave('photos_results/rm_dilated.png', img, cmap = 'gray')
    img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, np.ones((1,1),'uint8'))
    plt.imsave('photos_results/rm_morph1.png', img, cmap = 'gray')
    r, thresh = cv2.threshold(img_in,50,200,0)
    plt.imsave('photos_results/rm_threshold.png', thresh, cmap = 'gray')
    img_out = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, np.ones((3,3),'uint8'))
    plt.imsave('photos_results/rm_morph2.png', img_out, cmap = 'gray')
    #img_out = cv2.dilate(img_out,kernel,iterations = 1)
    # isolate edges
    #canny_out, canny_down = AAF(canny_erode, downsample_factor)
    #img = cv2.Canny(np.uint8(img_in), 50, 110,3)
    return img_out



# ---------------------------------- Get Contours ----------------------------------
def getContours(img):
    im2, contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # order contours longest to shortest
    num_contours = len(contours)
    
    print('number of contours') # REMOVE FOR TIMING ANALYSIS
    print(num_contours) # REMOVE FOR TIMING ANALYSIS
    
    # initialize array with first contour's length
    cnt_lens = len(contours[0])
    # get length of remaining contours
    for n in range(1,num_contours-1):
        cnt_lens = np.hstack([cnt_lens,-1*len(contours[n])]) # mult by -1 so that sort makes ascending order be descending magnitude
    
    # sort from longest to shortest contour
    descending_order = np.argsort(cnt_lens) # returns original index of the sorted value

    #sorted_lengths = np.sort(cnt_lens) # returns the contours lengths from (longest to shortest)*-1
    #print('sorted contour lengths')
    #print(sorted_lengths)

    # build new list of arrays of coordinates -- keep only those larger than threshold min length
    sorted_contours = [contours[descending_order[0]], contours[descending_order[1]]]
    for m in range(2,num_contours-1):
        cnt = contours[descending_order[m]]
        if(len(cnt) > 70): # 50 seems to be a good number so far
            sorted_contours.extend([cnt])

    print('length of sorted contours') # REMOVE FOR TIMING ANALYSIS
    print(len(sorted_contours)) # REMOVE FOR TIMING ANALYSIS

    return sorted_contours



# --------------------------------- Draw Contours ----------------------------------
def drawConts(sorted_contours,img,img0):
    
    # DRAW ALL CONTOURS ON ORIGINAL IMAGE
    cv2.drawContours(img0, sorted_contours, -1, (0,255,0), 2)
    #plt.imsave('photos_results/with_contours.png', img0)

    tmp = np.shape(img0)
    mask3 = np.zeros((tmp[0], tmp[1],3), np.uint8)
    cv2.drawContours(mask3, sorted_contours, -1, (0,255,0), 2)
    plt.imsave('expo/final_cnt_on_blank.png', mask3)

# ------------------------------------- Send Contours --------------------------------
def sendContours(contours):
    print('starting transmission to Arduino')
    
    #start_clk = time.clock() # GET TIMING INFO *****
    num_contours = len(contours)
    delay_len = 0.8 # in seconds
    
    # tell the Arduino to prepare for a new image to be sent
    new_pic_available = 'n'
    ser.write(new_pic_available.encode())
    sleep(delay_len)
    final_end = 'd'
    counter = 0
    
    # send all contours pixel by pixel
    for n in range(0,num_contours): # loop contours
        #start_cnt = time.clock() # GET TIMING INFO *****
        cnt = contours[n]
        cnt_len = len(cnt)
        begin_char = 'b'
        end_char = 'e'
        ser.write(begin_char.encode())
        print(n)
        counter = 0
        #sleep(2)
        for m in range(0,cnt_len): # loop pixels
            #start_pix = time.clock() # GET TIMING INFO *****
            # adaptive transmisison time -- longer delay for first 10 coordinates drawn
            if(counter>=10):
                if(cnt_len < 70):
                    if(m % 2 == 0 or m == cnt_len-1):
                        str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                        ser.write(str_msg.encode())
                        sleep(0.25)
                        counter = counter+1
                elif(cnt_len < 200):
                    if(m % 5 == 0 or m == cnt_len-1):
                        str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                        ser.write(str_msg.encode())
                        sleep(0.25)
                        counter = counter+1
            
                elif(m % 10 == 0 or m == cnt_len-1):
                    str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                    ser.write(str_msg.encode())
                    sleep(0.25)
                    counter = counter+1
            else:
                if(cnt_len < 70):
                    if(m % 2 == 0 or m == cnt_len-1):
                        str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                        ser.write(str_msg.encode())
                        sleep(1)
                        counter = counter+1
                elif(cnt_len < 200):
                    if(m % 5 == 0 or m == cnt_len-1):
                        str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                        ser.write(str_msg.encode())
                        sleep(1)
                        counter = counter+1
                elif(m % 2 == 0 or m == cnt_len-1):
                    str_msg = '{}.{},'.format(cnt[m][0][0]*2,cnt[m][0][1]*2)
                    ser.write(str_msg.encode())
                    sleep(0.8)
                    counter = counter+1
            #print time.clock() - start_pix, "seconds to send pixel" # GET TIMING INFO *****
        
      
        ser.write(end_char.encode())
        counter = 0
        #print time.clock() - start_cnt, "seconds to send contour" # GET TIMING INFO *****
        sleep(2.5)
       
    print(n)
    ser.write(final_end.encode())
    #print time.clock() - start_clk, "seconds to send all contours" # GET TIMING INFO *****

# ----------------------------------- Save Images ------------------------------------
def saveFinalImage(edge_orig, final_im, filter_name):
    plt.imsave('photos_results/final_' + filter_name + '.png', final_im, cmap = 'gray')
    plt.imsave('photos_results/final_pre_' + filter_name + '.png', edge_orig, cmap = 'gray')


# ----------------------------------- Image Processing ------------------------------------
def processImg(at_home, image_name, take_new):

    start_time = time.clock()
    
    # take picture with rasPi Camera
    img0 = captureImage(image_name,at_home,take_new)
    
    # converting to gray scale and convert to floating point number 0-1
    im_gray = cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)

    # detect edges -- Options: sobel, laplacian, prewitt, cannyIm
    edge = applyEdgeDetection(im_gray)
    
    # Remove Nosie  -- threshold and erode/dilate
    final_im = removeNoise(edge, img0)
    
    # save final image
    #saveFinalImage(edge, final_im, 'filtered')

    # evaluate contours
    sorted_contours = getContours(final_im)
    drawConts(sorted_contours, final_im, img0)

    # get run time
    print (str(time.clock() - start_time) + " seconds to execute image processing")
    print("image processing finished")
    
    print('Press button to send image contours')
    sleep(2)
    if at_home == False:
        while(True):    
            input_state3 = GPIO.input(14) #button 2
            #input_state2 = GPIO.input(13)
            if(input_state3 == False):
                sendContours(sorted_contours)
                break
        
    #sendContours(sorted_contours)



#----------------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------- Main -----------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------
def main():
    at_home = False # UPDATE WHEN TAKING PICTURES *******
    take_new = False # UPDATE
    date = '13' # UPDATE WHEN TAKING PICTURES *******
    counter = 13 # UPDATE WHEN TAKING PICTURES *******
    
    # for working on image processing without pi (uses pre-saved images)
    if(at_home == True):
        #image_name = 'circle.jpg'
        #image_name = 'test_button_press.jpg'
        #image_name = 'Izzy_test8.jpg'
        #mage_name = 'Thaer.jpg'   # 1 = Izzy, 2 = Meghna, 3 = Thaer, 8 = Blake, 9 = Basil, 10 = Socrates
        #image_name = 'TEST_LIGHTING.jpg'
        image_name = 'presentation_Demo.jpg'
        processImg(at_home, image_name)
    
    # for working with access to the pi camera and robot
    else:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(14, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        print('ready: press button to preview camera window')
        while(True):
            # take picture with raspPi Camera
            #img_name = 'april' + str(date) +'_test' + str(counter) + '.jpg'
            #img_name = 'TEST_LIGHTING.jpg'
            image_name = 'profKimDemo.jpg'
            input_state = GPIO.input(14)
            if input_state == False:
                processImg(at_home, image_name, take_new)
                counter = counter + 1
                print('ready: press button to preview camera window')

if __name__ == "__main__":
    main()



