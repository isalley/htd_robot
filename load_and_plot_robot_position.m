%% load robot position based on contours
close all

num_contours = 3;
x = [];
y = [];
x_coor = [];
y_coor = [];
zero_pad = zeros(1,10);
y_pad = ones(1,10)*400;

for n = 1:num_contours
    n
    clear pixel_locationY
    clear pixel_locationX
    if n == 1
        % zig zags
        pixel_locationY = [400,225:-1:175,175:1:200,201:-1:150,150:200,y_pad];
        len = numel(pixel_locationY);
        pixel_locationX = [0,20:96,96:172,172:-1:147,zero_pad];
    elseif n == 2
        % circle
        radius=10;
        center=[90 237];
        theta=0:2*pi/360:2*pi;
        x_circ = radius*cos(theta)+center(1);
        y_circ = radius*sin(theta)+center(2);
        
        pixel_locationY = [400,y_circ,y_pad];
        len = numel(pixel_locationY);
        pixel_locationX = [0,x_circ,zero_pad];
        
    elseif n == 3
        % square corners
        pixel_locationY = [400, 50:69, 69*ones(1,10), 69:-1:50, 50*ones(1,10),50:69, y_pad];
        len = numel(pixel_locationY);
        pixel_locationX = [0, 100*ones(1,20), 100:109, 109*ones(1,20), 109:118, 118*ones(1,20), zero_pad];
    end
    
    % plot input
    figure(1)
    subplot(2,1,1);plot(pixel_locationX, 400-pixel_locationY,'b*-'); title('Input');
    hold on
    axis([0 400 0 400]);
    x_coor = [x_coor pixel_locationX];
    y_coor = [y_coor pixel_locationY];
    length(x_coor)
    
    % scale pixel location to be an angluar position
    A = 360/400; % gain from contour location to angular motor position
    B = 8/360; % gain from angular position to real space inches position
    control_inputY = (400-pixel_locationY)*A;
    control_inputX = pixel_locationX*A;
    length(control_inputX)

    % build transfer function
    s = tf('s');
    G = 1/(5*s+1)/(1*s+1); % damp the system
    H = 1; % sensor gain
    G_loop = G;%G/(1+H*G);

    % apply transfer function to the x and y coordinates
    control_outputX = lsim(G_loop, control_inputX, 0:length(control_inputX)-1);
    control_outputY = lsim(G_loop, control_inputY, 0:length(control_inputX)-1);
    length(control_outputX)


    % saturate values so that the robot is not asked to move to a noexistant
    % position
    maxY = 360;
    maxX= 360;
    for n = 1:length(control_outputX)
        if control_outputY(n) > maxY
            control_outputY(n) = maxY;
        elseif control_outputY(n) < 0
            control_outputY(n) = 0;
        end
        if control_outputX(n) > maxX
            control_outputX(n) = maxX;
        elseif control_outputX(n) < 0
            control_outputX(n) = 0;
        end
    end

    % scale to be real space coordinate
    real_spaceX = control_outputX'*B;
    real_spaceY = control_outputY'*B;

    % add the coordinates to the travel path
    x = [x real_spaceX];
    y = [y real_spaceY];
    length(x)

end

%% plot output of control of robot's position
figure(1)
subplot(2,1,2); plot(x, y, 'r*-'); xlabel('X Position');ylabel('Y Position'); title('Control Output');
axis([0 8 0 8]);
print('Overall_control','-dpng');

figure(2)
subplot(2,1,1);plot(1:length(y_coor), 400-y_coor,'b*-'); ylabel('Input X Coordinate');
subplot(2,1,2); plot(1:length(y), y, 'r*-'); xlabel('Time Index');ylabel('Control Output X Coordinate');
print('X_control','-dpng');

figure(3)
subplot(2,1,1);plot(1:length(x_coor), x_coor,'b*-'); ylabel('Input Y Coordinate');
subplot(2,1,2); plot(1:length(x),x, 'r*-'); xlabel('Time Index'); ylabel('Control Output Y Coordinate');
print('Y_control','-dpng');

