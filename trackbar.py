import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import time
from time import sleep


#cap.set(3,600)
#cap.set(4,600)
# start video stream
cv2.namedWindow('Camera_View')
cv2.namedWindow('Contours')
#cap = cv2.VideoCapture(-1)

def nothing(x):
    pass

# create slider bars
cv2.createTrackbar('Brightness','Camera_View',70,100, nothing)
cv2.createTrackbar('Contrast','Camera_View',20,200, nothing)
cv2.createTrackbar('Saturation','Camera_View',50,200, nothing)
cv2.createTrackbar('Contour Minimum Length Threshold','Camera_View',50,200, nothing)



def main():
    frame = np.zeros((500, 500,3), np.uint8)
    # display image processing feedback
    while(1):
        #_, frame = cap.read()
        cv2.imshow('Camera_View',frame)
        cv2.waitKey(1000)
        # get info from track bar and appy to result
        brightness = cv2.getTrackbarPos('Brightness','Camera_View')
        contrast = cv2.getTrackbarPos('Contrast','Camera_View')
        saturation = cv2.getTrackbarPos('Saturation','Camera_View')
        min_cnt_length = cv2.getTrackbarPos('ContLenMin','Camera_View')
        
        # trackbars do not offer negative numbers as slider values
        contrast = 100 - contrast
        saturation = 100 - saturation
        print('brightness:', brightness)
        print('contrast:', contrast)
        print('saturation:', saturation)
        cv2.imshow('Camera_View',frame)
        cv2.waitKey(1000)

    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
